# ARCalibration

## Description

This repository contains the source code necessary to build **ARCalibration** applications as *git submodules*.

## Build instructions

Generic instructions for fw4spl applications can be found here:
 - [stable version](http://fw4spl.readthedocs.org/en/master/Installation/index.html)
 - [latest version](http://fw4spl.readthedocs.org/en/dev/Installation/index.html)

To setup this specific repository you can adapt the following `cmake` command:

```
cmake -G "Ninja" -DCMAKE_BUILD_TYPE:STRING="Release" -DBUILD_TESTS:BOOL=OFF -DPROJECTS_TO_BUILD:STRING="ARCalibration;charucoBoard" -DCMAKE_INSTALL_PREFIX:PATH=/path/to/ARCalibration/install/$DIR -DEXTERNAL_LIBRARIES:PATH=/path/to/sight-deps/install/$DIR /path/to/ARCalibration/sight
```
